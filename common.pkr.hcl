

locals {
  runtime_usr  = "drone"
  runtime_grp  = "drones"
  runtime_uid  = 1000
  runtime_gid  = 1000
  runtime_home = "/opt/app"
  maintainer   = "oss@unimatrixone.io"
  docker_env   = [
    "ENV RUNTIME_USR=${local.runtime_usr}",
    "ENV RUNTIME UID=${local.runtime_uid}",
    "ENV RUNTIME_GRP=${local.runtime_grp}",
    "ENV RUNTIME GID=${local.runtime_gid}",
    "ENV RUNTIME_HOME=${local.runtime_home}",
  ]
}
