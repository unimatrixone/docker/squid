# Squid Docker Image

This Packer template builds a Docker image with Squid configured to use
Logstash or `/dev/stdout`. Its purpose is to provide monitoring of egress
HTTP traffic. Caching is not configured.

# Usage

```
docker run -it registry.gitlab.com/unimatrixone/docker/squid:latest
```

# Environment variables


---

# Building

1.  Install the following prequisites:

    - Docker
    - Packer v1.6.1 or higher (build only).
    - Push access to the configured Docker repository (build only).

2.  Populate the file `local.pkr.hcl` with the appropriate values:

    ```
    locals {
      docker_repository = "<replace with your Docker repository>"
    }
    ```
    Alternatively, the Docker repository may be provided through the command
    line with the `-var 'docker_repository='"<your Docker repository>"'` parameter
    (note the quoting).

3.  Run:

    ```
    packer build .
    ```


# License

MIT/BSD


# Author information

This template was created by **Cochise Ruhulessin** for the
[Unimatrix One](https://cloud.unimatrixone.io) platform.

- [Send me an email](mailto:cochise.ruhulessin@unimatrixone.io)
- [GitHub](https://github.com/cochiseruhulessin)
- [LinkedIn](https://www.linkedin.com/in/cochise-ruhulessin-0b48358a/)
