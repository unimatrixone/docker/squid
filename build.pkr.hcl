

build {
  sources = [
    "source.docker.latest",
  ]

  provisioner "shell" {
    inline = [
        "apk add --no-cache squid",
      ]
  }

  provisioner "file" {
    source = "etc/squid.conf"
    destination = "/etc/squid/squid.conf"
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = ["latest"]
      only        = ["docker.latest"]
    }

    post-processor "docker-push" {}
  }
}

