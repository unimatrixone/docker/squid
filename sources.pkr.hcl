

source "docker" "latest" {
  image   = "registry.gitlab.com/unimatrixone/docker/alpine:latest"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "WORKDIR ${local.runtime_home}",
    "USER 1000",
    "ENTRYPOINT [\"squid\"]",
    "CMD [\"--foreground\", \"-d 1\"]"
  ]
}
